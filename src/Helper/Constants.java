package Helper;

public class Constants {
	public static final String	GRID_NAME				= "AllAgentsGrid";
	public static final String	CONTEXT					= "DynamicWarehouse";
	public static final String	GRID_HEIGHT				= "GridHeight";
	public static final String	GRID_WIDTH				= "GridWidth";
	public static final String	DELIVERY_FREQUENCY		= "GoodDeliveryFrequency";
	public static final String	MAX_STORAGE_TIME			= "MaxStorageTime";
	public static final String	MIN_STORAGE_TIME			= "MinStorageTime";
	public static final Integer	DAY_IN_TICKS				= 1440;
	public static final String	AMOUNT_ROBOTS			= "AmountRobot";
	public static final String	AMOUNT_ENTRY_PORTALS		= "EntryPortal";
	public static final String	AMOUNT_PICKUP_PORTALS	= "PickupPortal";
	public static final String	STORAGE_STRATEGY			= "StorageStrategy";
}

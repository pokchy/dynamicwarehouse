package Helper;

public class Node {
	
	// Class variables
	
	private int		finalCost;
	private int		movementCost;
	private int		movementCounts;
	private int		x;
	private int		y;
	private boolean	isBlock;
	private Node		parent;

	// Constructor
	
	public Node(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * Method that calculates the minimal movementCounts from this node to destinationPoint.
	 * @param finalNode
	 */
	public void calculateHeuristic(Node destinationNode) {
		this.movementCounts = Math.abs(destinationNode.getX() - this.getX()) + Math.abs(destinationNode.getY() - this.getY());
	}

	/**
	 * Method that sets all Node Data by setting the parentNode and calculating the g and f cost.
	 * @param currentNode
	 * @param cost
	 */
	public void setNodeData(Node currentNode, int cost) {
		// Kosten summieren sich über die Strecke hinweg
		int movementCost = currentNode.getMovementCost() + cost;
		setParent(currentNode);
		setMovementCost(movementCost);
		calculateFinalCost();
	}

	/**
	 * Method that checks if currentNode is the better path option by comparing the total costs.
	 * @param currentNode
	 * @param cost
	 * @return Boolean
	 */
	public boolean checkBetterPath(Node currentNode, int cost) {
		int movementCost = currentNode.getMovementCost() + cost;
		if ( movementCost < getMovementCost() ) {
			setNodeData(currentNode, cost);
			return true;
		}
		// (...)
		return false;
	}

	/**
	 * Method that calculates the final cost by adding the H Cost to the G Cost.
	 */
	private void calculateFinalCost() {
		int finalCost = getMovementCost() + getMovementCounts();
		setFinalCost(finalCost);
	}

	@Override
	public boolean equals(Object arg0) {
		Node other = (Node) arg0;
		return this.getX() == other.getX() && this.getY() == other.getY();
	}

	@Override
	public String toString() {
		return "Node [x=" + x + ", y=" + y + " = " + isBlock + "]";
	}
	
	// Getter/Setter methods
	
	public int getMovementCounts() {
		return movementCounts;
	}

	public void setMovementCounts(int movementCounts) {
		this.movementCounts = movementCounts;
	}

	public int getMovementCost() {
		return movementCost;
	}

	public void setMovementCost(int movementCost) {
		this.movementCost = movementCost;
	}

	public int getFinalCost() {
		return finalCost;
	}

	public void setFinalCost(int finalCost) {
		this.finalCost = finalCost;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public boolean isBlock() {
		return isBlock;
	}

	public void setBlock(boolean isBlock) {
		this.isBlock = isBlock;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
}
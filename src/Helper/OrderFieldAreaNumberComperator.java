package Helper;

import java.util.Comparator;

import Model.IMyAgent;
import Model.OrderField;

public class OrderFieldAreaNumberComperator implements Comparator<IMyAgent> {
	
	@Override
	public int compare(IMyAgent o1, IMyAgent o2) {
		
		// First by AreaNumber
		int areaNumberResult = ((OrderField) o1).getAreaNumber() < ((OrderField) o2).getAreaNumber() ? -1
				: ((OrderField) o1).getAreaNumber() > ((OrderField) o2).getAreaNumber() ? 1 : 0;
		if ( areaNumberResult != 0 ) {
			return areaNumberResult;
		}
		
		// Next by x
		int xCoordinateResult = ((OrderField) o1).getGridpoint().getX() < ((OrderField) o2).getGridpoint().getX() ? -1
				: ((OrderField) o1).getGridpoint().getX() > ((OrderField) o2).getGridpoint().getX() ? 1 : 0;
		if ( xCoordinateResult != 0 ) {
			return xCoordinateResult;
		}
		
		// Finally by y
		return ((OrderField) o1).getGridpoint().getY() < ((OrderField) o2).getGridpoint().getY() ? -1
				: ((OrderField) o1).getGridpoint().getY() > ((OrderField) o2).getGridpoint().getY() ? 1 : 0;
	}

}
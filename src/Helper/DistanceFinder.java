package Helper;

import Model.IMyAgent;
import Model.PickupPortal;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;

public class DistanceFinder {
	public static Grid<IMyAgent> grid;

	public static int getShortestDistanceBetweenAgents(IMyAgent agent1, IMyAgent agent2) {
		return Math.abs(agent1.getGridpoint().getX() - agent2.getGridpoint().getX())
				+ Math.abs(agent1.getGridpoint().getY() - agent2.getGridpoint().getY());
	}

	public static int getShortestDistanceBetweenAgentAndGridPoint(IMyAgent agent1, GridPoint agent2) {
		return Math.abs(agent1.getGridpoint().getX() - agent2.getX()) + Math.abs(agent1.getGridpoint().getY() - agent2.getY());
	}

	public static int getDistanceToClosestPickupPortal(GridPoint gridPoint) {
		int closest = 0;
		int temp = 0;
		for ( IMyAgent agent : AgentFinder.getAllObjectsOfType(PickupPortal.class) ) {
			temp = getShortestDistanceBetweenAgentAndGridPoint(agent, gridPoint);
			if ( closest == 0 ) {
				closest = temp;
			} else {
				if ( closest > temp ) {
					closest = temp;
				}
			}
		}
		return closest;
	}
}
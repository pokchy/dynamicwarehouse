package Helper;

import java.util.ArrayList;

import Model.IMyAgent;
import Model.Robot;
import repast.simphony.space.grid.Grid;

public class AgentFinder {
	
	// Instance variable
	
	public static Grid<IMyAgent> grid;

	/**
	 * Method that takes an instance of IMyAgent.
	 * It gets the class of that instance and calls the second method of the class,
	 * which return a list of all Objects of this specified class/type.
	 * @param instance
	 * @return list
	 */
	public static ArrayList<IMyAgent> getAllObjectsOfType(IMyAgent instance) {
		return getAllObjectsOfType(instance.getClass());
	}

	/**
	 * Method that gives back a list of all objects in the simulation that are the same type.
	 * @param c
	 * @return
	 */
	public static ArrayList<IMyAgent> getAllObjectsOfType(Class<?> c) {
		ArrayList<IMyAgent> list = new ArrayList<IMyAgent>();
		// Iterate list of agents on the grid
		for ( IMyAgent agent : grid.getObjects() ) {
			if ( agent.getClass().equals(c) ) {
				list.add((IMyAgent) agent);
			}
		}
		// Return Arraylist
		return list;
	}
	
	
	/**
	 * This method gives back an object at a certain of
	 * the grid if it corresponds to the parameter class.
	 * @param c		class of object
	 * @param x		x-coordinate
	 * @param y		y-coordinate
	 * @return object or null
	 */
	public static IMyAgent getObjectOfType(Class<?> c, int x, int y) {
		// Iterate list of agents on the grid
		for ( IMyAgent object : grid.getObjectsAt(x, y) ) {
			// Test if found object corresponds to Class c
			if ( object.getClass().equals(c) ) {
				// Return the found, classified object of Class c
				return object;
			}
		}
		// Return null
		return null;
	}

	/**
	 * Method that return the closest Robot object which is free, means: has no job
	 * aka is RobotJobType.None in that very moment
	 * @param agent
	 * @return closest (Robot)
	 */
	public static Robot getClosestFreeRobot(IMyAgent agent) {
		Robot closest = null;
		Robot temp = null;
		for ( IMyAgent object : getAllObjectsOfType(Robot.class) ) {
			if ( ((Robot) object).getRobotJobType().equals(Robot.RobotJobType.NONE) ) {
				temp = (Robot) object;
				if ( closest == null ) {
					closest = temp;
				} else {
					int distanceClosest = DistanceFinder.getShortestDistanceBetweenAgents(closest, agent);
					int distanceTemp = DistanceFinder.getShortestDistanceBetweenAgents(temp, agent);
					if ( distanceClosest > distanceTemp ) {
						closest = temp;
					}
				}
			}
		}
		// Return closest
		return closest;
	}
}
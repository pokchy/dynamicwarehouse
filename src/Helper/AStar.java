package Helper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import Model.IMyAgent;
import Model.Robot;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

/**
 * Source & inspiration
 * https://www.geeksforgeeks.org/a-search-algorithm
 */

public class AStar {
	
	// Instance & class variables
	
	private int					hvCost			= 10;
	private Node[][]				searchArea;
	private PriorityQueue<Node>	openList;
	private ArrayList<Node>		closedList;
	private Node					initialNode;
	private Node					destinationNode;

	// Constructor
	
	public AStar(Node initialNode, Node destinationNode) {
		Parameters p = RunEnvironment.getInstance().getParameters();
		this.initialNode = initialNode;
		this.destinationNode = destinationNode;
		this.searchArea = new Node[p.getInteger(Constants.GRID_WIDTH)][p.getInteger(Constants.GRID_HEIGHT)];
		this.closedList = new ArrayList<Node>();
		this.openList = new PriorityQueue<Node>(new Comparator<Node>() {
			@Override
			// sortiert nach FinalCost
			// kleinere Wert steht vorne
			public int compare(Node node0, Node node1) {
				return node0.getFinalCost() < node1.getFinalCost() ? -1 : node0.getFinalCost() > node1.getFinalCost() ? 1 : 0;
			}
		});
		this.setNodes();
		this.setBlocks();
	}

	/**
	 * Method that initializes the searchArray with nodes.
	 * (...)
	 */
	private void setNodes() {
		for ( int i = 0; i < searchArea.length; i++ ) {
			for ( int j = 0; j < searchArea[0].length; j++ ) {
				Node node = new Node(i, j);
				node.calculateHeuristic(getDestinationNode());
				this.searchArea[i][j] = node;
			}
		}
	}

	/**
	 * Method that set blocks for each cell that is occupied by a robot.
	 * @param agentArray
	 */
	private void setBlocks() {
		for ( IMyAgent o : AgentFinder.getAllObjectsOfType(Robot.class) ) {
			this.setBlock(o.getGridpoint().getX(), o.getGridpoint().getY());
		}
	}
	
	/**
	 * Method that iterates the openList and returns next step of the (currently) shortest path.
	 * @return
	 */
	public Node findPath() {
		this.openList.add(initialNode);
		// Ongoind loop for finding the best path.
		while ( !isEmpty(openList) ) {
			// Take the first entry of the List and remove it.
			// openList is always sorted.
			Node currentNode = this.openList.poll();
			// (...)
			this.closedList.add(currentNode);
			// (...)
			if ( this.isFinalNode(currentNode) ) {
				return this.getNextStep(currentNode);
			// (...)
			} else {
			// Path building happens here.
			// Test if one way is better than another one.
			// Path is saved within the openList.
				this.addNeighborhoodNodes(currentNode);
			}
		}
		// (...)
		return null;
	}

	/**
	 * Iterates all parents of nodes and add them to an arrayList returns the node
	 * for the next step
	 * 
	 * @param currentNode
	 * @return node (for next step)
	 */ 
	private Node getNextStep(Node currentNode) {
		Node node = null;
		List<Node> path = new ArrayList<Node>();
		path.add(currentNode);
		Node parent;
		while ( (parent = currentNode.getParent()) != null ) {
			path.add(0, parent);
			currentNode = parent;
		}
		if ( path.size() > 1 ) {
			node = path.get(1); // get First step of path
		}
		// ...
		return node;
	}
	
	/**
	 * Summary/super method that checks all the neighboorhood Nodes
	 * @param currentNode
	 */
	private void addNeighborhoodNodes(Node currentNode) {
		this.addNeighborhoodRightX(currentNode);
		this.addNeighborhoodMiddleX(currentNode);
		this.addNeighborhoodLeftX(currentNode);
	}

	/**
	 * Method that checks right neighbor Node
	 * @param currentNode
	 */
	private void addNeighborhoodRightX(Node currentNode) {
		int x = currentNode.getX();
		int y = currentNode.getY();
		int lowerX = x + 1;
		// (...)
		if ( lowerX < this.getSearchArea().length ) {
			this.checkNode(currentNode, y, lowerX, this.getHvCost());
		}
	}

	/**
	 * Method that checks the vertical Nodes (Neighbor to the top and bottom)
	 * @param currentNode
	 */
	private void addNeighborhoodMiddleX(Node currentNode) {
		int x = currentNode.getX();
		int y = currentNode.getY();
		int middleX = x;
		if ( y - 1 >= 0 ) {
			this.checkNode(currentNode, y - 1, middleX, this.getHvCost());
		}
		if ( y + 1 < getSearchArea()[0].length ) {
			this.checkNode(currentNode, y + 1, middleX, this.getHvCost());
		}
	}

	/**
	 * Method that checks left neighbor Node
	 * @param currentNode
	 */
	private void addNeighborhoodLeftX(Node currentNode) {
		int x = currentNode.getX();
		int y = currentNode.getY();
		int upperX = x - 1;
		if ( upperX >= 0 ) {
			this.checkNode(currentNode, y, upperX, this.getHvCost());
		}
	}

	/**
	 * Method that checks Node (if it's not blocked or in closedList)
	 * 
	 * @param currentNode
	 * @param y				- y coordinate (to generate neighbor)
	 * @param x				- x coordinate (to generate neighbor)
	 * @param cost			- cost variable
	 */
	private void checkNode(Node currentNode, int y, int x, int cost) {
		Node neighborhoodNode = this.getSearchArea()[x][y];
		if ( !neighborhoodNode.isBlock() && !getClosedList().contains(neighborhoodNode) ) {
			if ( !this.getOpenList().contains(neighborhoodNode) ) {
				neighborhoodNode.setNodeData(currentNode, cost);
				this.getOpenList().add(neighborhoodNode);
			} else {
				// (...)
				boolean changed = neighborhoodNode.checkBetterPath(currentNode, cost);
				if ( changed ) {
					// Example case: (...)
					// Remove and Add the changed node, so that the PriorityQueue can sort again its
					// contents with the modified "finalCost" value of the modified node
					this.getOpenList().remove(neighborhoodNode);
					this.getOpenList().add(neighborhoodNode);
				}
			}
		}
	}
	
	// Getter/Setter methods
	
	private boolean isFinalNode(Node currentNode) {
		return currentNode.equals(destinationNode);
	}

	private boolean isEmpty(PriorityQueue<Node> openList) {
		return openList.size() == 0;
	}

	private void setBlock(int x, int y) {
		this.searchArea[x][y].setBlock(true);
	}

	public Node getInitialNode() {
		return initialNode;
	}

	public void setInitialNode(Node initialNode) {
		this.initialNode = initialNode;
	}

	public Node getDestinationNode() {
		return destinationNode;
	}

	public void setDestinationlNode(Node destinationNode) {
		this.destinationNode = destinationNode;
	}

	public Node[][] getSearchArea() {
		return searchArea;
	}

	public void setSearchArea(Node[][] searchArea) {
		this.searchArea = searchArea;
	}

	public PriorityQueue<Node> getOpenList() {
		return openList;
	}

	public void setOpenList(PriorityQueue<Node> openList) {
		this.openList = openList;
	}

	public List<Node> getClosedList() {
		return closedList;
	}

	public void setClosedList(ArrayList<Node> closedList) {
		this.closedList = closedList;
	}

	public int getHvCost() {
		return hvCost;
	}

	public void setHvCost(int hvCost) {
		this.hvCost = hvCost;
	}
}
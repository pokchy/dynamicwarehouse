package Helper;

import java.util.Comparator;
import java.util.PriorityQueue;

import Model.Good;

public class GoodStorageSystem {
	
	private static GoodStorageSystem	single_instance	= null;
	
	private static PriorityQueue<Good>	entryQueue		= new PriorityQueue<Good>(new Comparator<Good>() {
															@Override
															public int compare(Good good0, Good good1) {
																return good0.getEntryTime() < good1.getEntryTime() ? -1
																		: good0.getEntryTime() > good1.getEntryTime()
																				? 1
																				: 0;
															}
														});
	
	private static PriorityQueue<Good>	pickupQueue		= new PriorityQueue<Good>(new Comparator<Good>() {
															@Override
															public int compare(Good good0, Good good1) {
																return good0.getPickupTime() < good1.getPickupTime()
																		? -1
																		: good0.getPickupTime() > good1.getPickupTime()
																				? 1
																				: 0;
															}
														});

	/**
	 * Singleton
	 * @return
	 */
	public static GoodStorageSystem getInstance() {
		if ( single_instance == null ) {
			single_instance = new GoodStorageSystem();
		}
		// 
		return single_instance;
	}

	public PriorityQueue<Good> getEntryQueue() {
		return entryQueue;
	}

	public void setEntryQueue(PriorityQueue<Good> entryQueue) {
		GoodStorageSystem.entryQueue = entryQueue;
	}

	public PriorityQueue<Good> getPickupQueue() {
		return pickupQueue;
	}

	public void setPickupQueue(PriorityQueue<Good> pickupQueue) {
		GoodStorageSystem.pickupQueue = pickupQueue;
	}
}

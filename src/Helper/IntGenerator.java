package Helper;

public class IntGenerator {
	/**
	 * Generate a random number in range of min and max. Min and Max are included in
	 * the result
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int generateRandomWithRange(int min, int max) {
		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}
	
}
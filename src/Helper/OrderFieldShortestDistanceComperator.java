package Helper;

import java.util.Comparator;

import Model.IMyAgent;
import Model.OrderField;

public class OrderFieldShortestDistanceComperator implements Comparator<IMyAgent> {
	@Override
	public int compare(IMyAgent o1, IMyAgent o2) {
		
		// First by Distance
		int distanceResult = ((OrderField) o1).getMinimalTravleDistance() < ((OrderField) o2).getMinimalTravleDistance()
				? -1
				: ((OrderField) o1).getMinimalTravleDistance() > ((OrderField) o2).getMinimalTravleDistance() ? 1 : 0;
		if ( distanceResult != 0 ) {
			return distanceResult;
		}
		
		// Next by x
		int xCoordinateResult = ((OrderField) o1).getGridpoint().getX() < ((OrderField) o2).getGridpoint().getX() ? -1
				: ((OrderField) o1).getGridpoint().getX() > ((OrderField) o2).getGridpoint().getX() ? 1 : 0;
		if ( xCoordinateResult != 0 ) {
			return xCoordinateResult;
		}
		
		// Finally by y
		return ((OrderField) o1).getGridpoint().getY() < ((OrderField) o2).getGridpoint().getY() ? -1
				: ((OrderField) o1).getGridpoint().getY() > ((OrderField) o2).getGridpoint().getY() ? 1 : 0;
	}
	
}
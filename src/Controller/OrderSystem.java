package Controller;

import java.util.ArrayList;
import java.util.Collections;

import Helper.AgentFinder;
import Helper.Constants;
import Helper.OrderFieldAreaNumberComperator;
import Helper.OrderFieldShortestDistanceComperator;
import Model.IMyAgent;
import Model.OrderField;
import Model.Portal;
import Model.Robot;
import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;

public class OrderSystem {
	
	// Instance & class variables
	private static Grid<IMyAgent>		grid;
	private static Context<IMyAgent>		context;
	private int							height;
	private int							width;
	private static boolean				fieldsAreSet		= false;
	
	/**
	 * Constructor without parameters
	 * For the case of storage strategy 1
	 */
	public OrderSystem() {
		Parameters params = RunEnvironment.getInstance().getParameters();
		this.height = params.getInteger(Constants.GRID_HEIGHT);
		this.width = params.getInteger(Constants.GRID_WIDTH);
		if ( !this.areFieldsSet() ) {
			this.setFields();
			this.setLinearStorageAreas();
		}
	}

	/** Constructor without parameters
	 *  For the case of storage strategy 2 or 3
	 * @param grid
	 * @param context
	 */
	public OrderSystem(Grid<IMyAgent> grid, Context<IMyAgent> context) {
		OrderSystem.grid = grid;
		OrderSystem.context = context;
		OrderSystem.fieldsAreSet = false;
	}

	/**
	 * Method that initializes the orderField with Fields
	 * Necessary for storage strategy 1
	 */
	public void setFields() {
		this.setFieldsAreSet(true);
		int ammountFields = this.height * this.width;
		for ( int i = 0; i < ammountFields; i++ ) {
			int x = i % this.width;
			int y = i / this.width;
			IMyAgent agent = grid.getObjectAt(x, y);
			OrderField field = new OrderField(x, y);
			// No portal should be a storage area
			if ( !(agent instanceof Portal) ) {
				context.add(field);
				grid.moveTo(field, x, y);
				field.calculateHeuristicToClosestPickupPortal();
			} else {
				field.setStorageArea(false);
				field.setMinimalTravleDistance(0);
				// AreaNumber = 0 is a portal, no storage area
				field.setAreaNumber(0);
			}
		}
	}

	/**
	 * Method that assigns a storageDuration to every OrderField within in grid.
	 * This is important if the user decided for storage strategy 1.
	 */
	public void setLinearStorageAreas() {
		// Set storage duration for every field on the grid
		Parameters params = RunEnvironment.getInstance().getParameters();
		int maxStorage = params.getInteger(Constants.MAX_STORAGE_TIME);
		ArrayList<IMyAgent> allFields = AgentFinder.getAllObjectsOfType(OrderField.class);
		Collections.sort(allFields, new OrderFieldAreaNumberComperator());
		int lineareStorageProportion = allFields.size() / maxStorage;
		int areaNumber = 1;
		int counter = 1;
		for ( IMyAgent agent : allFields ) {
			((OrderField) agent).setAreaNumber(areaNumber);
			if ( areaNumber < maxStorage && counter % lineareStorageProportion == 0 ) {
				areaNumber++;
			}
			counter++;
		}
	}

	/**
	 * Method that gives back an ArrayList with all free OrderField of a certain type.
	 * @param orderNumber
	 * @return fieldList
	 */
	public ArrayList<OrderField> getAllFreeFieldsByAreaNumber(int orderNumber) {
		ArrayList<OrderField> fieldList = new ArrayList<OrderField>();
		for ( IMyAgent agent : AgentFinder.getAllObjectsOfType(OrderField.class) ) {
			OrderField orderField = (OrderField) agent;
			if ( orderField.getAreaNumber() == orderNumber && orderField.isStorageArea() && !orderField.isApproached()
					&& !orderField.isOccupied() ) {
				fieldList.add(orderField);
			}
		}
		return fieldList;
	}

	/**
	 * Method that returns one object of class OrderField, a free OrderField
	 * (...)
	 * @param orderNumber
	 * @return orderField
	 */
	public OrderField getFreeFieldByAreaNumber(int orderNumber) {
		OrderField orderField = null;
		ArrayList<IMyAgent> allFields = AgentFinder.getAllObjectsOfType(OrderField.class);
		Collections.sort(allFields, new OrderFieldShortestDistanceComperator());
		for ( IMyAgent agent : allFields ) {
			OrderField temp = (OrderField) agent;
			Robot robotOnField = (Robot) AgentFinder.getObjectOfType(Robot.class, temp.getGridpoint().getX(),
					temp.getGridpoint().getY());
			if ( temp != null && robotOnField == null && temp.isStorageArea() && !temp.isApproached()
					&& !temp.isOccupied() ) {
				if ( (temp.getAreaNumber() == orderNumber || (temp.getAreaNumber() == 1 && orderNumber < 1)) ) {
					orderField = temp;
					break;
				}
			}
		}
		//...
		return orderField;
	}

	/**
	 * Method that returns all currently free OrderFields in form of an ArrayList("fieldList")
	 * @return fieldList
	 */
	public ArrayList<OrderField> getAllFreeFields() {
		ArrayList<OrderField> fieldList = new ArrayList<OrderField>();
		for ( IMyAgent agent : AgentFinder.getAllObjectsOfType(OrderField.class) ) {
			OrderField orderField = (OrderField) agent;
			Robot robotOnField = (Robot) AgentFinder.getObjectOfType(Robot.class, orderField.getGridpoint().getX(), orderField.getGridpoint().getY());
			if ( robotOnField == null && orderField.isStorageArea() && !orderField.isApproached() && !orderField.isOccupied() ) {
				fieldList.add(orderField);
			}
		}
		// Returns the ArrayList with free OrderFields
		return fieldList;
	}
	
	// Getter/Setter methods

	public Grid<IMyAgent> getG() {
		return grid;
	}

	public void setG(Grid<IMyAgent> grid) {
		OrderSystem.grid = grid;
	}

	public boolean areFieldsSet() {
		return fieldsAreSet;
	}

	public void setFieldsAreSet(boolean fieldsAreSet) {
		OrderSystem.fieldsAreSet = fieldsAreSet;
	}
}

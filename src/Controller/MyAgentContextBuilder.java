package Controller;

import Helper.AgentFinder;
import Helper.Constants;
import Helper.DistanceFinder;
import Helper.GoodStorageSystem;
import Helper.Node;
import Model.EntryPortal;
import Model.IMyAgent;
import Model.PickupPortal;
import Model.Robot;
import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.StrictBorders;

public class MyAgentContextBuilder implements ContextBuilder<IMyAgent> {
	
	// Instance variables
	
	private Grid<IMyAgent>	grid			= null;
	private int				height		= 0;
	private int				width		= 0;
	private GoodGenerator	generator	= null;
	private Parameters		params		= null;
	
	/*
	 * Method that builts a context object by the use of the parameters setup by the user or default values.
	 * Builds e. g. the grid, adds robots and portals and initiates helper classes.
	 * Returns a context object.
	 */
	@Override
	public Context<IMyAgent> build(Context<IMyAgent> context) {
		
		/*
		 * The context objekt represents a group of agents that participate in a simulation.
		 * A Context ecapsulates a population of agents.
		 */
		this.params = RunEnvironment.getInstance().getParameters();
		this.height = params.getInteger(Constants.GRID_HEIGHT);
		this.width = params.getInteger(Constants.GRID_WIDTH);
		// (...)
		GridBuilderParameters<IMyAgent> gridBuilderParameters = new GridBuilderParameters<IMyAgent>(new StrictBorders(),
				new RandomGridAdder<IMyAgent>(), true, width, height);
		this.grid = (Grid<IMyAgent>) GridFactoryFinder.createGridFactory(null).createGrid(Constants.GRID_NAME, context,
				gridBuilderParameters);

		// Initiate the GoodGenerator 
		this.generator = new GoodGenerator();
		this.generator.activateGoodGenerator();

		// Add agents to the grid
		this.addRobotToGrid(context);
		this.addPickupPortalToGrid(context);
		this.addEntryPortalToGrid(context);

		// Initiates helper classes (see package "Helper")
		AgentFinder.grid = this.grid;
		DistanceFinder.grid = this.grid;
		GoodStorageSystem.getInstance().getEntryQueue().clear();
		GoodStorageSystem.getInstance().getPickupQueue().clear();
		new OrderSystem(this.grid, context);
		
		// Returns the created context object
		return context;
	}

	/**
	 * Method that adds one or more EntryPortals to the grid of our context.
	 * EntryPortals are placed at the north east border of the grid.
	 * @param context
	 */
	private void addEntryPortalToGrid(Context<IMyAgent> context) {
		// Put maximum amount of EntryPortals at the north and east border of the grid
		int maxNumber = this.width + this.height - 1;
		if ( this.params.getInteger(Constants.AMOUNT_ENTRY_PORTALS) < maxNumber ) {
			maxNumber = this.params.getInteger(Constants.AMOUNT_ENTRY_PORTALS);
		}
		for ( int i = 0; i < maxNumber; i++ ) {
			EntryPortal entryPortal = new EntryPortal();
			context.add(entryPortal);
			if ( i < this.width ) {
				this.grid.moveTo(entryPortal, i, this.height - 1);
			} else {
				this.grid.moveTo(entryPortal, this.width - 1, this.height - 2 - (i % this.height));
			}
		}
	}

	/**
	 * Method that adds PickupPortals to grid.
	 * Works like "addEntryPortalToGrid", but puts the PickupPortals at the west/south border of the grid.
	 * @param context
	 */
	private void addPickupPortalToGrid(Context<IMyAgent> context) {
		// Put maximum amount of PickupPortals at the west and south border of the grid.
		int maxNumber = this.width + this.height - 3;
		if ( this.params.getInteger(Constants.AMOUNT_PICKUP_PORTALS) < maxNumber ) {
			maxNumber = this.params.getInteger(Constants.AMOUNT_PICKUP_PORTALS);
		}
		// Loops through amount of to added PickupPortals, adds them, and moves between grid position.
		for ( int i = 0; i < maxNumber; i++ ) {
			PickupPortal pickupPortal = new PickupPortal();
			context.add(pickupPortal);
			if ( i < this.width - 1 ) {
				this.grid.moveTo(pickupPortal, i, 0);
			} else {
				this.grid.moveTo(pickupPortal, 0, ((i + 2) % this.width));
			}
		}
	}

	/**
	 * Method that adds robots to the grid of our context.
	 * @param context
	 */
	private void addRobotToGrid(Context<IMyAgent> context) {
		// Add maximum amount of Robots on grid on a random, non border place
		int maxNumber = this.width * this.height;
		if ( this.params.getInteger(Constants.AMOUNT_ROBOTS) < maxNumber ) {
			maxNumber = this.params.getInteger(Constants.AMOUNT_ROBOTS);
		}
		// Creates max number of robots (set by user or use the default value)
		// Also give them a start point and save/set it in instance variable
		for ( int i = 0; i < maxNumber; i++ ) {
			Robot robot = new Robot();
			context.add(robot);
			while ( this.grid.getLocation(robot).getX() == 0 || this.grid.getLocation(robot).getX() == this.width -1 || this.grid.getLocation(robot).getY() == 0 || this.grid.getLocation(robot).getY() == this.height -1) {
				context.remove(robot);
				context.add(robot);
			}
			// Set start point within the grid of each robot
			robot.setStartPoint(
					new Node(this.grid.getLocation(robot).getX(), this.grid.getLocation(robot).getY()));
		}
	}
}
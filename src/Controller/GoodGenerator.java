package Controller;

import Helper.Constants;
import Helper.GoodStorageSystem;
import Model.Good;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;

public class GoodGenerator {
	
	/**
	 * Method that activates the GoodGenerator with the help of ScheduleParameters object.
	 */
	public void activateGoodGenerator() {
		// Parameters describing when an action should be scheduled.
		ScheduleParameters sp = ScheduleParameters.createRepeating(1, Constants.DAY_IN_TICKS);
		// Provides access to the environment in which a particular model run executes.
		// E.g., the schedule is made available to running simulation code in this way.
		RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, "generateNewDeliveryGoods");
	}

	/**
	 * Method that generates new delivery good every day and adds them to the EntryQueue.
	 */
	public void generateNewDeliveryGoods() {
		// Create Paramters object, gets parameters from running simulation (RunEnvironment)
		Parameters params = RunEnvironment.getInstance().getParameters();
		// Set the delivery frequency
		int deliveryFrequency = params.getInteger(Constants.DELIVERY_FREQUENCY);
		// Loop that creates new goods and adds them to the EntryQueue of the GoodStorageSystem.
		for ( int i = 0; i < deliveryFrequency; i++ ) {
			Good good = new Good();
			GoodStorageSystem.getInstance().getEntryQueue().add(good);
		}
	}
}
package View;

import java.awt.Color;

import Helper.Constants;
import Model.OrderField;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class MyFieldStyle extends DefaultStyleOGL2D {
	
	/**
	 * Method that creates a rectangle form with size 15*15
	 */
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		this.shapeFactory.createRectangle(15, 15, true);
		// Return (...)
		return super.getVSpatial(agent, spatial);
	}

	/**
	 * Method that gives back Color object for OrderField
	 * Actual color depends on choosed storage strategy and areaNumber
	 */
	@Override
	public Color getColor(Object agent) {
		int red = 255;
		int green = 255;
		int blue = 255;
		Color color = null;
		Parameters params = RunEnvironment.getInstance().getParameters();
		Integer strategy = params.getInteger(Constants.STORAGE_STRATEGY);
		if (agent instanceof OrderField && strategy != 2 && strategy != 3) {
			red = red - (10 * ((OrderField) agent).getAreaNumber());
			if ( red < 0 ) {
				red = 0;
				green = (green + 255) - (10 * ((OrderField) agent).getAreaNumber());
				if ( green < 0 ) {
					green = 0;
					blue = (blue + 255) - (10 * ((OrderField) agent).getAreaNumber());
					if ( blue < 0 ) {
						blue = 0;
					}
				}
			}
		}
		color = new Color(red, green, blue);
		// Return Color object
		return color;
	}
}
package View;

import java.awt.Color;

import Model.EntryPortal;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class MyEntryPortalStyle extends DefaultStyleOGL2D {
	
	/**
	 * Method that creates a rectangle form with size 14*14
	 */
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if ( spatial == null ) {
			spatial = shapeFactory.createRectangle(14, 14);
		}
		// Returns (...)
		return super.getVSpatial(agent, spatial);
	}
	
	/**
	 * Method that gives back Color object, actual color depends on status of the EntryPortal
	 * Color can be Yellow (occupied) or Green (not occupied)
	 */
	@Override
	public Color getColor(Object agent) {
		Color color = null;
		if ( agent instanceof EntryPortal ) {
			if ( ((EntryPortal) agent).isOccupied() == true ) {
				color = new Color(237, 234, 36); // Yellow
			} else {
				color = new Color(78, 184, 163); // Green
			}
		}
		// Return Color object
		return color;
	}
}
package View;

import java.awt.Color;

import Model.Good;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class MyGoodStyle extends DefaultStyleOGL2D {
	
	/**
	 * Method that creates a circle form with size 5*5
	 */
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if ( spatial == null ) {
			spatial = shapeFactory.createCircle(5, 5, true);
		}
		// Return (...)
		return super.getVSpatial(agent, spatial);
	}
	
	/**
	 * Method that gives back a Color object.
	 * Actual color depends on the Good's current storageDuration.
	 */
	@Override
	public Color getColor(Object agent) {
		Color color = null;
		if ( agent instanceof Good ) {
			if ( ((Good) agent).getStoreDuration() == 1 ) {
				color = Color.green;
			} else if ( ((Good) agent).getStoreDuration() == 2 ) {
				color = Color.yellow;
			} else if ( ((Good) agent).getStoreDuration() == 3 ) {
				color = Color.orange;
			} else if ( ((Good) agent).getStoreDuration() >= 4 ) {
				color = Color.red;
			} else {
				color = Color.blue;
			}
		}
		// Return Color object
		return color;
	}
}
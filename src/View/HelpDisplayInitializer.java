package View;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import repast.simphony.context.Context;
import repast.simphony.engine.controller.NullAbstractControllerAction;
import repast.simphony.engine.environment.GUIRegistryType;
import repast.simphony.engine.environment.RunEnvironmentBuilder;
import repast.simphony.engine.environment.RunState;
import repast.simphony.parameter.Parameters;
import repast.simphony.scenario.ModelInitializer;
import repast.simphony.scenario.Scenario;
import repast.simphony.ui.RSApplication;

/**
 * The "Erläuterungen & Hilfe" display serves as an expansion to the UserPanel, 
 * where a first introduction to the program and its functional principle is given to the user.
 * 
 * In this display we explain in detail things like:
 * 		- concept and case of the simulation
 * 		- its functionality
 * 		- the options of the simulation
 * 		- the parameters
 * 		- results of the simulation
 * 
 * Should there be anymore open questions, we will link to our full-length documentary
 * of the simulation programm in form of a PDF document.
 * 
 * The UserPanel is also created here, see below.
 */

public class HelpDisplayInitializer implements ModelInitializer {
	
	/** 
	 * Method that initializes the "Erläuterungen & Hilfe" display
	 * It uses different JavaSwing element such as JPanels and JLabels,
	 * placed within a GridBagLayout
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void initialize(Scenario scen, RunEnvironmentBuilder builder) {
		// Create customized JPanel for Display "Erläuterungen & Hilfe"
		JPanel jp = new JPanel();
		// Header image / Logo
		JLabel labelHeader = new JLabel();
		// Agenten image
		JLabel labelAgentsImage = new JLabel();
		// Text labels
		JLabel labelTextCase = new JLabel();
		JLabel labelTextParameter = new JLabel();
		JLabel labelTextAgenten = new JLabel();
		JLabel labelTextStrategien = new JLabel();
		JLabel labelTextVorgehen = new JLabel();
		JLabel labelTextHilfe = new JLabel();
		// HTML blocks
		String htmlStart = "<html><body width='550px'>";
		String htmlEnd = "</body></html>";
		// Text blocks
		String textCase = "<h3>Case & Funktionsweise</h3>"
				+ "Programmierbare <b>Roboter</b> in einem Lagerhaus, sogenannte <b>WarehouseBots</b>, helfen dabei, die Abwicklung des <b>Lagervorgangs</b> diverser <b>Güter</b> möglichst effizient zu gestalten. Die Roboter bewegen sich in dem <b>rechteckigen</b> Lagerraum <b>horizontal</b> und <b>vertikal</b> auf <b>gitterartig</b> angelegten Schienen und sind dabei in der Lage, die Waren/Goods von Punkt A nach B zu befördern. Neben einfachen <b>Stellflächen</b> im Lager gibt es noch zwei Portale: das <b>Wareneingangs-Portal</b> und das <b>Warenausgangs-Portal</b>.";
		String textParameter = "<h3>Simulations-Parameter</h3>"
				+ "Die Größe und das Verhältnis der Seiten des Lagerraums können geändert werden. Mit der Simulation und den Information zum Warendurchlauf können mehrere Faktoren so bestimmt werden, dass die Lagerung der Güter möglichst schnell bzw. reibungsfrei vonstatten geht. Dazu zählen u. a. die Größe der Lagerfläche, die Anzahl der einzusetzenden Roboter, der Sortiermechanismus. Es folgt eine genaue Auslistung der veränderbaren Parameter:"
				+ "<br/><br/><u>Anzahl Roboter</u>:<br/> Die Anzahl der WarehouseBots, welche im Lager aktiv sind, Waren bewegen und sortieren."
				+ "<br/><br/><u>Anzahl der Warenannahmestellen</u>:<br/> Bei z.B. 3 Warenannahmestellen werden im Lager drei Stellflächen zur Bildung des Wareneingangs-Portals genutzt."
				+ "<br/><br/><u>Anzahl der Warenausgabestellen</u>:<br/> Im Prinzip genauso wie bei den Warenannahmestellen, allerdings wird hier das Warenausgangs-Portal gebildet."
				+ "<br/><br/><u>Default Random Seed</u>: (ist zu ignorieren)"
				+ "<br/><br/><u>Grit Höhe</u>:<br/> Die Höhe der Lagerfläche, also die Anzahl der Stellflächen vertikal gesehen."
				+ "<br/><br/><u>Grit Weite</u>:<br/> Die Breite der Lagerfläche, also die Anzahl der Stellflächen horizontal gesehen."
				+ "<br/><br/><u>Lagerungsstrategie</u>:<br/> Es kann zwischen drei verschiedenen Lagerungsstrategien gewählt werden (zu bestimmen mit der Eingabe von 1, 2 oder 3). Wie die Strategien jeweils genau im Einzelnen funktionieren, steht unter dem Punkt 'Drei Strategien zur Lagerung' erklärt, siehe weiter unten."
				+ "<br/><br/><u>Maximale Lagerzeit in Tagen</u>:<br/> Die maximale Lagerzeit, welche Waren annehmen können."
				+ "<br/><br/><u>Minimale Lagerungszeit</u>:<br/> Die minimale Lagerzeit, die eine Ware aufweisen muss."
				+ "<br/><br/><u>Warenannahme pro Tag</u>:<br/> Wie viele Pakete/Goods das Lager pro Tag erreichen.";
		String textAgenten = "<h3>Die Agenten</h3>"
				+ "Im folgenden werden die Agenten der Simulation vorgestellt bzw. deren Aufgabe erklärt."
				+ "<br/><br/><u>Roboter/WarehouseBots</u>:<br/>Die Roboter dienen dazu, Pakete vom Wareneingang in das Lager zu bringen, und später von dort aus zum Warenausgang. Sie werden durch ein Roboter-Icon repräsentiert."
				+ "<br/><br/><u>Waren/Goods/Pakete</u>: Pakete gelangen durch das Wareneingangs-Portal in das Lager, wo sie von den WarehouseBots auf Stellflächen abgestellt werden und später zum Warenausgangs-Portal gebracht werden, wo sie das Lager wieder verlassen. Sie verfügen über einen Warenlagerzeitraum. Die Farbe der Goods verändert sich entsprechend ihrer aktuell verbleibenden Lagerzeit. Ist eine Ware blau, ist die Warenlagerungszeit abgelaufen."
				+ "<br/><br/><u>Wareneingangs- und Warenausgangs-Portal</u>: Durch diese beiden Portal gelangen die Waren/Paket entweder in das Lager oder auch wieder hinaus. Sie können zudem verschiedene Status und dementsprechend Farben annehmen. Ist ein Feld des Wareneingangs-Portals gelb, kann die Ware von einem Roboter abgeholt werden. Ist es grün, gibt es an dieser Stelle gerade nichts zu tun. Beim Warenausgangs-Portal verhält es sich so: Ist ein Feld grau, muss eine Ware gebracht werden. Ist es schwarz, gibt es im Moment nichts zu tun."
				+ "<br/><br/><u>Grid / Lagerfläche</u>: Dieses ist gitterartig angeordnet und bietet Platz für die Portale und Waren. Außerdem bewegen sich auf ihm die WarehouseBots. Wurde die Strategie 1 ausgewählt, färben sich die Lagerflächen, je nach ihrer Funktion als Lagerzeitplatzhalter. Dabei werden dunkler gefärbte Lagerflächen für Waren mit höheren Lagerzeiten zum Abstellen genutzt.";
		String textStrategien = "<h3>Drei Strategien zur Lagerung</h3>"
				+ "Im Parameter-Panel kann zwischen drei verschieneden Strategien bzw. <b>Sortier-Algorithmen</b> zur Lagerung der Waren/Pakete gewählt werden. Diese werden im Folgenden kurz vorgestellt:<br><br>"
				+ "<u>Lagerstrategie 1:</u><br/>Bei dieser Strategie sortieren die WarehouseBots die Pakete so, dass die Pakete mit größten Wartedauer am weitesten entfernt vom Warenausgangs-Portal gelagert werden. Parallel dazu sind die Stellflächen des Lagers in z.B. 15 Bereiche (entsprechend des maximal angegebenen Lagerzeitraums der Waren) eingesteilt und dementsprechend mit Farben von weiß (Lagerzeitraum der Ware sehr niedrig) bis blau (Lagerzeitraum der Ware sehr hoch) versehen. Der Roboter überprüft also die Wartezeit einer Ware und sortiert sie dementsprechend zum optimalen Platz (schafft sie zu einem Lagerfeld). Nach 1440 Ticks (entspricht 1440 Minuten bzw. 1 Tag) verändert sich die Lagerzeit einer Ware um -1. Der Ablauf eines Tages führt auch zu einer Neusortierung im Lager, es geht für die Ware also eine Welle nach vorne. Ist eine Welle bereits voll, kommt das Paket einfach in die nächst höhere Welle. Bei diesem Sortieralgorithmus gibt es die Prioritäten der WarehouseBots zu beachten: Die höchste Priorität genießt der Warenausgang, dieser wird stets zuerst bedient. Danach folgt an zweiter Stelle der Wareneingang bzw. das Wareneingangs-Portal. Und zuletzt (an dritter Stelle) erfolgt dann die Sortierung  der Ware. Eine solche Prioritätenliste ist bei den anderen beiden Sortierstrategien nicht gegeben."
				+ "<br/><br/><u>Lagerstrategie 2:</u><br/> Bei dieser Strategie werden die Pakete einfach so nah wie möglich an das Warenausgangs-Portal gebracht."
				+ "<br/><br/><u>Lagerstrategie 3:</u><br/> Dieser Lagerstrategie liegt kein spezifischer Algorithmus zugrunde, vielmehr werden die Paket einfach zufällig im Lager verteilt. Auch hier spielt die Priorität der Sortierung keine Rolle."
				+ "<br/><br/>Befindet sich ein WarehouseBot ohne zugewiesenen Auftrag auf einem Punkt im Lager, der nicht seinem Startpunkt entspricht, kehrt er zu eben jenem automatisch zurück.";
		String textVorgehen = "<h3>Vorgehen & Ergebnis der Simulation</h3>"
				+ "Kommt eine neue Ware ins Lager, erreicht sie das <b>Wareneingangs-Portal</b>. Von dort wird sie von einem WarehouseBot abgeholt und auf eine freie Stellfläche gebracht. Basis dafür kann ein Sortiermechanismus sein. Danach wird die Ware zum <b>Warenausgangs-Portal</b> gebracht und aus dem Lager entfernt. Da die Simulation mit verschiedenen <b>Parameter-Settings</b> durchgeführt werden kann, wird geschaut, was zu einem optimalen Ergebnis führt. So kann eine Auswertung der <b>durchschnittlichen Wartezeit für Warenausgang und -eingang</b> stattfinden (siehe entsprechendes Display).";
		String textHilfe = "<h3>Weitere Hilfe & Dokumentation</h3>"
				+ "Sollten weitere Fragen zur WarehouseBots-Simulation bestehen, verweisen wir auf unser GitLab-Projekt, wo auch die Dokumentation zum Projekt als PDF heruntergeladen werden kann:";
		// JButton gitLabLink
		JButton gitLink = new JButton("GitLab Projekt inkl. Dokumentation");
		// Set Layout of panel to GridBagLayout
		jp.setLayout(new GridBagLayout());
		// Set gbc and settings/options
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(5, 5, 5, 5);
		// Set grid/layout values to 0
		gbc.gridx = 0;
		gbc.gridy = 0;
		// Section 0 - HeaderImage / WarehouseBots Logo
		// Set image to JLabel
		URL url = null;
		try {
			url = new URL("http://www.gibhardt.de/gibhardt/temp/logo_small_wo_robot.png");
			
		} catch ( MalformedURLException e ) {
			e.printStackTrace();
		}
		labelHeader.setIcon(new ImageIcon(url));
		// Add JLabel labelHeader to panel
		jp.add(labelHeader, gbc);
		// Section - Konzept und Case
		// Next row
		gbc.gridy++;
		// Set content/text
		labelTextCase.setText(htmlStart + textCase + htmlEnd);
		// Add JLabel text1 to panel
		jp.add(labelTextCase, gbc);
		// Section - Funktionsweise
		// Next row
		gbc.gridy++;
		// Set content/text
		labelTextParameter.setText(htmlStart + textParameter + htmlEnd);
		// Add JLabel text2 to panel
		jp.add(labelTextParameter, gbc);
		// Section - Agenten
		// Next row
		gbc.gridy++;
		// Set content/text
		labelTextAgenten.setText(htmlStart + textAgenten + htmlEnd);
		// Add JLabel text2 to panel
		jp.add(labelTextAgenten, gbc);
		// Next row
		gbc.gridy++;
		// Set image to JLabel
		URL urlAgentImage = null;
		try {
			urlAgentImage = new URL("http://www.gibhardt.de/gibhardt/temp/warehouse_agents.png");
		} catch ( MalformedURLException e ) {
			e.printStackTrace();
		}
		labelAgentsImage.setIcon(new ImageIcon(urlAgentImage));
		// Add JLabel labelHeader to panel
		jp.add(labelAgentsImage, gbc);
		// Section - Strategien
		gbc.gridy++;
		// Set content/text
		labelTextStrategien.setText(htmlStart + textStrategien + htmlEnd);
		// Add JLabel text2 to panel
		jp.add(labelTextStrategien, gbc);
		// Section - Results of the simulation
		// Next row
		gbc.gridy++;
		// Set content/text
		labelTextVorgehen.setText(htmlStart + textVorgehen + htmlEnd);
		// Add JLabel text3 to panel
		jp.add(labelTextVorgehen, gbc);
		// Section - further help (link to GitLab) and documentation download (PDF)
		// Next row
		gbc.gridy++;
		// Set content/text
		labelTextHilfe.setText(htmlStart + textHilfe + htmlEnd);
		// Add JLabel text4 to panel
		jp.add(labelTextHilfe, gbc);
		// Section - JButton link to GitHub project
		gbc.gridy++;
		// ActionListener zu JButton gitLink hinzufügen
		gitLink.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					final URI gitURI = new URI("https://gitlab.com/pokchy/dynamicwarehouse");
					java.awt.Desktop.getDesktop().browse(gitURI);
				} catch ( IOException e1 ) {
					e1.printStackTrace();
				} catch ( URISyntaxException e1 ) {
					e1.printStackTrace();
				}
			}
		});
		// Set size of JButton
		gitLink.setPreferredSize(new Dimension(100, 40));
		gitLink.setMaximumSize(new Dimension(100, 40));
		// Set cursor
		gitLink.setCursor(new Cursor(Cursor.HAND_CURSOR));
		// Focus deactivate
		gitLink.setFocusPainted(false);
		// Add JButton gitLINK to panel
		jp.add(gitLink, gbc);
		// JScrollPane
		JScrollPane scrollPane = new JScrollPane(jp);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(50, 30, 300, 50);
		// Add custom JPanel to CustomUserPanel
		JPanel userPanel = createUserPanel();
		RSApplication.getRSApplicationInstance().addCustomUserPanel(userPanel);
		scen.addMasterControllerAction(new NullAbstractControllerAction() {
			@SuppressWarnings("unchecked")
			@Override
			public void runInitialize(RunState runState, Context context, Parameters runParams) {
				super.runInitialize(runState, context, runParams);
				runState.getGUIRegistry().addComponent(scrollPane, GUIRegistryType.OTHER, "Erläuterungen & Hilfe");
			}
		});
	}

	/**
	 * With this UserPanel of the RepastSimphony simulation we want to give a first and short
	 * introduction to the user of what's happening in the simulation. Furthermore there's a hint 
	 * to the "Erläuterungen & Hilfe" display at the end of the UserPanel.
	 * 
	 * @return JPanel jp 
	 */
	public JPanel createUserPanel() {
		// create customized JPanel for UserPanel
		JPanel jp = new JPanel();
		// Header image / Logo
		JLabel labelHeader = new JLabel();
		// Text labels
		JLabel labelText1 = new JLabel();
		JLabel labelText2 = new JLabel();
		JLabel labelText3 = new JLabel();
		// HTML blocks
		String htmlStart = "<html><body width='155px'>";
		String htmlEnd = "</body></html>";
		// Text blocks
		String text1 = "Diese <b>Simulation</b> soll auf Basis von <b>Repast Simphony</b> einen optimalen Einsatz von <b>WarehouseBots</b> für einen Kunden bestimmen. Welche Lagergröße eigenet sich, wie viele Roboter braucht man und welcher Sortiermechanismus führt zum besten Ergebnis?";
		String text2 = "<b>Funktionsweise</b><br/>"
				+ "Zur Beantwortung dieser Frage bietet die Simulation verschiedene <b>Parameter</b> an. Aus dem Ergebnis lässt sich  unter anderem ableiten, wie groß das (rechteckig gestaltete) Lager im besten Fall ist und wie die WarehouseBots zu programmieren sind - und wie viele zum Einsatz kommen.";
		String text3 = "Weiterführende Erklärungen und <b>Hilfe</b> (sowie den Download-Link zur <b>Dokumentation</b>) finden Sie unter der Karteikarte <b>Erläuterungen & Hilfe</b>.";
		// Set layout of panel to GridBagLayout
		jp.setLayout(new GridBagLayout());
		// Set gbc and settings/options
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(5, 5, 5, 5);
		// Set grid/layout values to 0 = reset layout
		gbc.gridx = 0;
		gbc.gridy = 0;
		// Section 0 - HeaderImage = WarehouseBots Logo
		// Set image to JLabel
		URL url = null;
		try {
			url = new URL("http://www.drift-ashore.de/temp/logo_xsmall_wo_robot.png");
		} catch ( MalformedURLException e ) {
			e.printStackTrace();
		}
		labelHeader.setIcon(new ImageIcon(url));
		// Add JLabel labelHeader to panel
		jp.add(labelHeader, gbc);
		// Section 1 - first text block
		// Next row
		gbc.gridy++;
		// Set content/text
		labelText1.setText(htmlStart + text1 + htmlEnd);
		// Add JLabel text1 to panel
		jp.add(labelText1, gbc);
		// Section 2 - second text block
		// Next row
		gbc.gridy++;
		// Set content/text
		labelText2.setText(htmlStart + text2 + htmlEnd);
		// Add JLabel text1 to panel
		jp.add(labelText2, gbc);
		// Section 3 - third text block
		// Next row
		gbc.gridy++;
		// Set content/text
		labelText3.setText(htmlStart + text3 + htmlEnd);
		// Add JLabel text1 to panel
		jp.add(labelText3, gbc);
		// Return finished UserPanel
		return jp;
	}
}
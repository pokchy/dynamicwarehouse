package View;

import java.awt.Color;
import Model.PickupPortal;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class MyPickupPortalStyle extends DefaultStyleOGL2D {
	
	/**
	 * Method that creates a rectangle form with size 14*14
	 */
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if ( spatial == null ) {
			spatial = shapeFactory.createRectangle(14, 14);
		}
		// Return (...)
		return super.getVSpatial(agent, spatial);
	}
	
	/**
	 * Method that gives back Color object, actual color depends on status of the PickupPortal
	 * Color can be Grey (occupied) or DarkGrey (not occupied)
	 */
	@Override
	public Color getColor(Object agent) {
		Color color = null;
		if ( agent instanceof PickupPortal ) {
			if ( ((PickupPortal) agent).isOccupied() ) {
				color = Color.GRAY;
			} else {
				color = Color.DARK_GRAY;
			}
		}
		// Return Color object
		return color;
	}
}

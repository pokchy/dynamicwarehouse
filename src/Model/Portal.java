package Model;

import Helper.AgentFinder;
import Helper.Constants;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.ui.probe.ProbedProperty;
import repast.simphony.util.ContextUtils;

public abstract class Portal implements IMyAgent {
	/**
	 * Holds the good that is either ready to Pickup or ready to store
	 */
	private Good good;
	/**
	 * Shows if Portal is occuoied (got a job to do) or not
	 */
	private boolean isOccupied;
	/**
	 * Shows if a robot is on the way to pick up the goods
	 */
	private boolean isApproachedByRobot;

	public Portal() {
		super();
		this.isOccupied = false;
	}

	@ScheduledMethod(start = 1, interval = 1, shuffle = false, duration = 0, priority = ScheduleParameters.FIRST_PRIORITY)
	public void step() {
		if ( !isOccupied() ) {
			this.dequeue();
		}
		if ( checkForJobOpportunity() ) {
			this.giveJobToRobot();
		}
	}
	
	/**
	 * Take the next order in queue and handle it according to to the kind of portal
	 */
	protected abstract void dequeue();

	/**
	 * Assign job types to robots depending on kind of portal
	 */
	protected abstract void giveJobToRobot();

	protected boolean checkForJobOpportunity() {
		return isOccupied() && !isApproachedByRobot() && AgentFinder.getObjectOfType(Robot.class, this.getGridpoint().getX(), this.getGridpoint().getY()) == null;
	}

	@ProbedProperty(displayName = "Auftrag vorhanden", usageName = "isOccupied", converter = "")
	public boolean isOccupied() {
		return isOccupied;
	}

	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}

	public Good getGood() {
		return good;
	}

	public void setGood(Good good) {
		this.good = good;
	}

	public GridPoint getGridpoint() {
		Grid<?> grid = (Grid<?>) ContextUtils.getContext(this).getProjection(Constants.GRID_NAME);
		return new GridPoint(grid.getLocation(this).getX(), grid.getLocation(this).getY());
	}

	public boolean isApproachedByRobot() {
		return isApproachedByRobot;
	}

	public void setApproachedByRobot(boolean isApproachedByRobot) {
		this.isApproachedByRobot = isApproachedByRobot;
	}
}
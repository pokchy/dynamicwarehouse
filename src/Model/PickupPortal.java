package Model;

import Helper.AgentFinder;
import Helper.GoodStorageSystem;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.ui.probe.ProbeID;

public class PickupPortal extends Portal {
	protected static long	agentIDCounter	= 1;
	protected long			agentID			= agentIDCounter++;
	private static long		numberOfDelivery;
	private static long		ticksOfWaiting;

	public PickupPortal() {
		super();
		PickupPortal.numberOfDelivery = 0;
		PickupPortal.ticksOfWaiting = 0;
	}

	/* (non-Javadoc)
	 * @see Model.Portal#dequeue()
	 */
	@Override
	public void dequeue() {
		if ( !GoodStorageSystem.getInstance().getPickupQueue().isEmpty() ) {
			if ( GoodStorageSystem.getInstance().getPickupQueue().peek().getPickupTime() <= RunEnvironment.getInstance()
					.getCurrentSchedule().getTickCount() ) {
				Good newGood = GoodStorageSystem.getInstance().getPickupQueue().poll();
				this.setGood(newGood);
				this.setOccupied(true);
			}
		}
	}

	@ProbeID()
	public String toString() {
		return "Warenausgang " + this.agentID;
	}

	@Override
	protected void giveJobToRobot() {
		Robot robot = AgentFinder.getClosestFreeRobot(this.getGood());
		if ( robot != null ) {
			robot.setRobotJobType(Robot.RobotJobType.PICKUP_JOB);
			robot.setPickupPortal(this);
			this.setApproachedByRobot(true);
		}
	}

	public long getAvgWaitingTime() {
		if ( PickupPortal.numberOfDelivery == 0 ) {
			return 0;
		}
		return PickupPortal.ticksOfWaiting / (PickupPortal.numberOfDelivery);
	}

	public void setWaitingTimeAndNumberOfPickups() {
		PickupPortal.numberOfDelivery++;
		PickupPortal.ticksOfWaiting += Math.abs(RunEnvironment.getInstance().getCurrentSchedule().getTickCount()
				- this.getGood().getPickupTime());
	}
}

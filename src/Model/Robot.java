package Model;

import java.util.ArrayList;

import Controller.OrderSystem;
import Helper.AStar;
import Helper.AgentFinder;
import Helper.Constants;
import Helper.GoodStorageSystem;
import Helper.IntGenerator;
import Helper.Node;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.ui.probe.ProbeID;
import repast.simphony.ui.probe.ProbedProperty;
import repast.simphony.util.ContextUtils;

public class Robot implements IMyAgent {
	
	// Instance & class variables
	
	protected static long	agentIDCounter	= 1;
	protected long			agentID;
	private Good				good;
	private EntryPortal		entryPortal;
	private PickupPortal		pickupPortal;
	private IMyAgent			targetAgent;
	private RobotJobType		robotJobType	= RobotJobType.NONE;
	private int				ticksNotMoved;
	private Node				startPoint;
	private Node				lastPoint;

	public enum RobotJobType {
		ENTRY_JOB, PICKUP_JOB, SORT_JOB, NONE
	}
	
	// Constructor
	
	public Robot() {
		super();
		this.agentID = agentIDCounter++;
		this.good = null;
		this.entryPortal = null;
		this.pickupPortal = null;
		this.robotJobType = RobotJobType.NONE;
		this.ticksNotMoved = 0;
	}

	/**
	 * If robot has an assigned job do the specific task or return to start point
	 */
	@ScheduledMethod(start = 1, interval = 1, shuffle = false, duration = 0, priority = ScheduleParameters.LAST_PRIORITY)
	public void step() {
		// Do the assigned job
		switch ( this.robotJobType ) {
			case PICKUP_JOB:
				if ( !this.goodIsCollected() ) {
					this.handlePickupGood();
				} else {
					this.deliverPickupGood();
				}
				break;
			case ENTRY_JOB:
				if ( !this.entryIsCollected() ) {
					this.handleEntryPortal();
				} else {
					Parameters params = RunEnvironment.getInstance().getParameters();
					Integer strategy = params.getInteger(Constants.STORAGE_STRATEGY);
					switch ( strategy ) {
						case 2:
							this.findFreeSpaceNearPickup();
							this.storeGood();
							break;
						case 3:
							this.findFreeRandomSpace();
							this.storeGood();
							break;
						default:
							this.findOrderedSpace();
							this.storeGood();
							break;
					}
				}
				break;
			case SORT_JOB:
				if ( !this.goodIsCollected() ) {
					this.handleGoodToSort();
				} else {
					this.findOrderedSpace();
					this.updateSortProgress();
				}
				break;
			case NONE:
				if ( this.checkReturn() ) {
					this.moveToGridpoint(this.startPoint);
				} else {
					this.addTicksToNotMoved();
				}
				break;
		}
	}

	// If robot did nothing for two ticks return to start point to not block other jobs
	private boolean checkReturn() {
		return (this.ticksNotMoved > 2);
	}

	/**
	 * Only add Ticks when a robot blocks the PickupPortal
	 */
	private void addTicksToNotMoved() {
		Grid<IMyAgent> grid = (Grid) ContextUtils.getContext(this).getProjection(Constants.GRID_NAME);
		if ( (grid.getObjectAt(this.getGridpoint().getX(), this.getGridpoint().getY()) instanceof PickupPortal) ) {
			this.ticksNotMoved++;
		}
	}

	// When assigned a job sorting job we know which entry portal to target
	private boolean entryIsCollected() {
		return (this.entryPortal == null);
	}

	// Robot arrived at entry portal for sorting job
	private void handleEntryPortal() {
		moveToAgent(this.entryPortal);
		if ( this.entryPortal.getGridpoint().equals(this.getGridpoint()) ) {
			this.loadGood(this.entryPortal);
		}
	}

	// Robot carries the good
	private boolean goodIsCollected() {
		return (this.good != null);
	}

	// Robot arrived at field with a stored good to deliver it to a pickup portal
	private void handlePickupGood() {
		Good good = this.pickupPortal.getGood();
		moveToAgent(good);
		if ( good.getGridpoint().equals(this.getGridpoint()) ) {
			this.loadGood(good);
		}
	}

	// Robot is doing the delivery job to pickup portal
	private void deliverPickupGood() {
		moveToAgent(this.pickupPortal);
		if ( this.pickupPortal.getGridpoint().equals(this.getGridpoint()) ) {
			this.unload();
		}
	}

	private void handleGoodToSort() {
		this.moveToAgent(this.targetAgent);
		if ( this.targetAgent.getGridpoint().equals(this.getGridpoint()) ) {
			this.loadGood(this.targetAgent);
		}
	}

	private void loadGood(IMyAgent agent) {
		if ( agent instanceof EntryPortal ) {
			this.good = this.entryPortal.getGood();
			this.entryPortal.setWaitingTimeAndNumberOfPickups();
			this.entryPortal.setGood(null);
			this.entryPortal.setOccupied(false);
			this.entryPortal.setApproachedByRobot(false);
			this.entryPortal = null;
		} else if ( agent instanceof Good ) {
			this.targetAgent = null;
			this.good = (Good) agent;
			ContextUtils.getContext(this).remove(this.good);
			OrderField orderField = ((OrderField) AgentFinder.getObjectOfType(OrderField.class,
					this.getGridpoint().getX(), this.getGridpoint().getY()));
			orderField.setApproached(false);
			orderField.setOccupied(false);
		}
	}

	private void unload() {
		Grid<IMyAgent> grid = (Grid) ContextUtils.getContext(this).getProjection(Constants.GRID_NAME);
		if ( this.robotJobType.equals(RobotJobType.ENTRY_JOB) || this.robotJobType.equals(RobotJobType.SORT_JOB) ) {
			ContextUtils.getContext(this).add(this.good);
			grid.moveTo(this.good, this.getGridpoint().getX(), this.getGridpoint().getY());
			GoodStorageSystem.getInstance().getPickupQueue().add(this.good);
		} else if ( this.robotJobType.equals(RobotJobType.PICKUP_JOB) ) {
			this.pickupPortal.setWaitingTimeAndNumberOfPickups();
			this.pickupPortal.setGood(null);
			this.pickupPortal.setApproachedByRobot(false);
			this.pickupPortal.setOccupied(false);
			this.pickupPortal = null;
		}
		this.ticksNotMoved = 0;
		this.good = null;
		this.robotJobType = RobotJobType.NONE;
		this.targetAgent = null;
	}

	private void updateSortProgress() {
		OrderField initialNode = (OrderField) AgentFinder.getObjectOfType(OrderField.class, this.getGridpoint().getX(),
				this.getGridpoint().getY());
		if ( this.targetAgent != null ) {
			if ( this.targetAgent.equals(initialNode) ) {
				this.good.setSortingInProgress(false);
				this.unload();
			} else {
				moveToAgent(this.targetAgent);
			}
		} else {
			this.good.setSortingInProgress(false);
			this.unload();
			this.ticksNotMoved = 0;
			this.robotJobType = RobotJobType.NONE;
		}
	}

	private void storeGood() {
		OrderField initialNode = (OrderField) AgentFinder.getObjectOfType(OrderField.class, this.getGridpoint().getX(),
				this.getGridpoint().getY());
		if ( this.targetAgent != null ) {
			if ( this.targetAgent.equals(initialNode) ) {
				this.unload();
			} else {
				this.moveToAgent(this.targetAgent);
			}
		} else {
			GoodStorageSystem.getInstance().getEntryQueue().add(this.good);
			this.ticksNotMoved = 0;
			this.good = null;
			this.robotJobType = RobotJobType.NONE;
		}
	}

	private void findOrderedSpace() {
		Parameters params = RunEnvironment.getInstance().getParameters();
		int maxStorage = params.getInteger(Constants.MAX_STORAGE_TIME);
		OrderSystem orderSystem = new OrderSystem();
		OrderField destinationNode = null;
		if ( this.targetAgent == null ) {
			for ( int i = 0; i <= maxStorage - this.good.getStoreDuration(); i++ ) {
				OrderField orderField = orderSystem.getFreeFieldByAreaNumber(this.good.getStoreDuration() + i);
				if ( orderField != null ) {
					destinationNode = orderField;
					orderField.setApproached(true);
					break;
				}
			}
			this.targetAgent = destinationNode;
		}
	}

	private void findFreeSpaceNearPickup() {
		Parameters params = RunEnvironment.getInstance().getParameters();
		int maxStorage = params.getInteger(Constants.MAX_STORAGE_TIME);
		OrderSystem orderSystem = new OrderSystem();
		if ( this.targetAgent == null ) {
			for ( int i = 0; i <= maxStorage; i++ ) {
				OrderField orderField = orderSystem.getFreeFieldByAreaNumber(i);
				if ( orderField != null ) {
					this.targetAgent = orderField;
					orderField.setApproached(true);
					break;
				}
			}
		}
	}

	private void findFreeRandomSpace() {
		OrderSystem orderSystem = new OrderSystem();
		if ( this.targetAgent == null ) {
			ArrayList<OrderField> freeFields = orderSystem.getAllFreeFields();
			if ( !freeFields.isEmpty() ) {
				OrderField field = freeFields.get(IntGenerator.generateRandomWithRange(0, freeFields.size() - 1));
				field.setApproached(true);
				this.targetAgent = field;
			}
		}
	}

	/**
	 * Move to Agent
	 * @param agent
	 */
	private void moveToAgent(IMyAgent agent) {
		Node destinationNode = new Node(agent.getGridpoint().getX(), agent.getGridpoint().getY());
		moveToGridpoint(destinationNode);
	}

	/**
	 * Move To GridPoint
	 * @param node
	 */
	private void moveToGridpoint(Node node) {
		Grid<IMyAgent> grid = (Grid) ContextUtils.getContext(this).getProjection(Constants.GRID_NAME);
		Node initialNode = new Node(this.getGridpoint().getX(), this.getGridpoint().getY());
		Node destinationNode = node;
		AStar aStar = new AStar(initialNode, destinationNode);
		Node path = aStar.findPath();
		if ( path != null ) {
			this.lastPoint = new Node(this.getGridpoint().getX(), this.getGridpoint().getY());
			grid.moveTo(this, path.getX(), path.getY());
			this.ticksNotMoved = 0;
		} else if ( this.checkReturn() ) {
			grid.moveTo(this, this.lastPoint.getX(), this.lastPoint.getY());
		} else {
			this.ticksNotMoved++;
		}
	}

	public GridPoint getGridpoint() {
		Grid<?> grid = (Grid) ContextUtils.getContext(this).getProjection(Constants.GRID_NAME);
		return new GridPoint(grid.getLocation(this).getX(), grid.getLocation(this).getY());
	}
	
	// Getter/Setter methods
	
	public long getAgentID() {
		return agentID;
	}

	public void setAgentID(long agentID) {
		this.agentID = agentID;
	}

	public Good getGood() {
		return good;
	}

	public void setGood(Good good) {
		this.good = good;
	}

	public IMyAgent getTargetAgent() {
		return targetAgent;
	}

	public void setTargetAgent(IMyAgent targetAgent) {
		this.targetAgent = targetAgent;
	}

	@ProbeID()
	public String toString() {
		return "Roboter " + this.agentID;
	}

	public EntryPortal getEntryPortal() {
		return entryPortal;
	}

	public void setEntryPortal(EntryPortal entryPortal) {
		this.entryPortal = entryPortal;
	}

	public PickupPortal getPickupPortal() {
		return pickupPortal;
	}

	public void setPickupPortal(PickupPortal pickupPortal) {
		this.pickupPortal = pickupPortal;
	}

	public RobotJobType getRobotJobType() {
		return robotJobType;
	}

	@ProbedProperty(displayName = "Job", usageName = "getRobotJobTypeToString", converter = "")
	public String getRobotJobTypeToString() {
		return this.robotJobType.toString();
	}

	public void setRobotJobType(RobotJobType robotJobType) {
		this.robotJobType = robotJobType;
	}

	@ProbedProperty(displayName = "Aktuelles Ziel", usageName = "getTarget", converter = "")
	public String getTarget() {
		String target = "";
		switch ( this.robotJobType ) {
			case PICKUP_JOB:
				if ( !this.goodIsCollected() ) {
					target = this.pickupPortal.getGood().getGridpoint().toString();
				} else {
					target = this.pickupPortal.getGridpoint().toString();
				}
				break;
			case ENTRY_JOB:
				if ( !this.entryIsCollected() ) {
					target = this.entryPortal.getGridpoint().toString();
				} else {
					if ( this.targetAgent == null ) {
						target = "Auf der Suche Nach Lagerfläche";
					} else {
						target = this.targetAgent.getGridpoint().toString();
					}
				}
				break;
			case SORT_JOB:
				if ( this.targetAgent == null ) {
					target = "Auf der Suche Nach Lagerfläche";
				} else {
					target = this.targetAgent.getGridpoint().toString();
				}
				break;
			case NONE:
				if ( this.checkReturn() ) {
					target = this.startPoint.toString();
				} else {
					target = "Warten";
				}
				break;
		}
		return target;
	}

	public int getTicksNotMoved() {
		return ticksNotMoved;
	}

	public void setTicksNotMoved(int ticksNotMoved) {
		this.ticksNotMoved = ticksNotMoved;
	}

	public Node getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Node startPoint) {
		this.startPoint = startPoint;
	}
}
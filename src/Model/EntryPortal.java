package Model;

import Helper.AgentFinder;
import Helper.GoodStorageSystem;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.ui.probe.ProbeID;

public class EntryPortal extends Portal {
	
	// Instance & class variables
	
	private static long	agentIDCounter	= 1;
	private long			agentID			= agentIDCounter++;
	private static long	numberOfPickups	= 0;
	private static long	ticksOfWaiting	= 0;
	
	// dequeue method
	@Override
	public void dequeue() {
		if ( !GoodStorageSystem.getInstance().getEntryQueue().isEmpty() ) {
			if ( GoodStorageSystem.getInstance().getEntryQueue().peek().getEntryTime() <= RunEnvironment.getInstance()
					.getCurrentSchedule().getTickCount() ) {
				this.setGood(GoodStorageSystem.getInstance().getEntryQueue().poll());
				this.setOccupied(true);
			}
		}
	}
	
	//
	@ProbeID()
	public String toString() {
		return "Wareneingang " + this.agentID;
	}

	
	/* (non-Javadoc)
	 * @see Model.Portal#giveJobToRobot()
	 */
	@Override
	protected void giveJobToRobot() {
		Robot robot = AgentFinder.getClosestFreeRobot(this);
		if ( robot != null ) {
			robot.setRobotJobType(Robot.RobotJobType.ENTRY_JOB);
			robot.setEntryPortal(this);
			this.setApproachedByRobot(true);
		}
	}
	
	/**
	 * @return long
	 */
	public long getAvgWaitingTime() {
		if ( EntryPortal.numberOfPickups == 0 ) {
			return 0;
		}
		return EntryPortal.ticksOfWaiting / (EntryPortal.numberOfPickups);
	}
	
	public void setWaitingTimeAndNumberOfPickups() {
		EntryPortal.numberOfPickups++;
		EntryPortal.ticksOfWaiting += Math.abs( RunEnvironment.getInstance().getCurrentSchedule().getTickCount()
				- this.getGood().getEntryTime());
	}
}
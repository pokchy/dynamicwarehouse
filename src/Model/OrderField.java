package Model;

import Helper.Constants;
import Helper.DistanceFinder;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.ui.probe.ProbeID;
import repast.simphony.ui.probe.ProbedProperty;
import repast.simphony.util.ContextUtils;

public class OrderField implements IMyAgent {
	
	protected static long	agentIDCounter	= 1;
	protected long			agentID;
	private boolean			isApproached;
	private boolean			isOccupied;
	private boolean			storageArea;
	private int				minimalTravleDistance;
	private int				areaNumber;
	
	// Constructor
	public OrderField(int x, int y) {
		this.agentID = agentIDCounter++;
		this.storageArea = true;
		this.isApproached = false;
		this.isOccupied = false;
	}


	/**
	 *  Calculates the minimal movementCounts from this node to destinationPoint
	 */
	public void calculateHeuristicToClosestPickupPortal() {
		this.minimalTravleDistance = DistanceFinder.getDistanceToClosestPickupPortal(
				new GridPoint(this.getGridpoint().getX(), this.getGridpoint().getY()));
		this.areaNumber = this.minimalTravleDistance;
	}
	
	// Getter/Setter
	
	@ProbedProperty(displayName = "Lagerfläche", usageName = "isStorageArea", converter = "")
	public boolean isStorageArea() {
		return storageArea;
	}

	public void setStorageArea(boolean storageArea) {
		this.storageArea = storageArea;
	}

	@ProbedProperty(displayName = "Von Robot angefahren", usageName = "isApproached", converter = "")
	public boolean isApproached() {
		return isApproached;
	}

	public void setApproached(boolean isApproached) {
		this.isApproached = isApproached;
	}

	@ProbedProperty(displayName = "Ware abgestellt", usageName = "isOccupied", converter = "")
	public boolean isOccupied() {
		return isOccupied;
	}

	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}

	@ProbedProperty(displayName = "Min. Distanz zu PickupPortal", usageName = "getMinimalTravleDistance", converter = "")
	public int getMinimalTravleDistance() {
		return minimalTravleDistance;
	}

	public void setMinimalTravleDistance(int minimalTraveDistance) {
		this.minimalTravleDistance = minimalTraveDistance;
	}

	@ProbedProperty(displayName = "Lagerflächentyp", usageName = "getAreaNumber", converter = "")
	public int getAreaNumber() {
		return areaNumber;
	}

	public void setAreaNumber(int areaNumber) {
		this.areaNumber = areaNumber;
	}

	@ProbeID()
	public String toString() {
		return "Lagerfeld " + this.agentID;
	}

	@Override
	public void step() {
	}

	@Override
	public GridPoint getGridpoint() {
		Grid<?> grid = (Grid) ContextUtils.getContext(this).getProjection(Constants.GRID_NAME);
		return new GridPoint(grid.getLocation(this).getX(), grid.getLocation(this).getY());
	}
}

package Model;

import repast.simphony.space.grid.GridPoint;

public interface IMyAgent {
	public void step();
	public GridPoint getGridpoint();
}
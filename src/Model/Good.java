package Model;

import Helper.AgentFinder;
import Helper.Constants;
import Helper.GoodStorageSystem;
import Helper.IntGenerator;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.ui.probe.ProbeID;
import repast.simphony.ui.probe.ProbedProperty;
import repast.simphony.util.ContextUtils;

public class Good implements IMyAgent {
	/**
	 * The unique identifier for the good
	 */
	protected static long	agentIDCounter		= 1;
	private long				id					= agentIDCounter++;
	/**
	 * The time in days, when the good is going to get picked up. eg. 2 means the
	 * good will be picked up in 2 Days
	 */
	private int				storeDuration;
	/**
	 * Represents the tick the good arrived at the entry-portal
	 */
	private long				entryTime;
	/**
	 * Represents the tick the good is going to be picked up. This variable gets
	 * calculated by the calculatePickupTime method.
	 */
	private long				pickupTime;
	private boolean			sortingInProgress	= false;

	/**
	 * Constructor
	 */
	public Good() {
		Parameters params = RunEnvironment.getInstance().getParameters();
		this.entryTime = (long) RunEnvironment.getInstance().getCurrentSchedule().getTickCount()
				+ IntGenerator.generateRandomWithRange(0, Constants.DAY_IN_TICKS - 1);
		this.storeDuration = IntGenerator.generateRandomWithRange(params.getInteger(Constants.MIN_STORAGE_TIME),
				params.getInteger(Constants.MAX_STORAGE_TIME));
		calculatePickupTime(params);
		ScheduleParameters sp = ScheduleParameters.createRepeating(this.entryTime + Constants.DAY_IN_TICKS,
				Constants.DAY_IN_TICKS);
		RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, "step");
	}

	/**
	 * Calculates the goods pickupTime once the object is created
	 * 
	 * @param p
	 */
	private void calculatePickupTime(Parameters p) {
		if ( this.storeDuration == 1 ) {
			this.pickupTime = this.entryTime
					+ IntGenerator.generateRandomWithRange(Constants.DAY_IN_TICKS / 2, Constants.DAY_IN_TICKS);
		} else {
			this.pickupTime = this.entryTime + ((getStoreDuration() - 1) * Constants.DAY_IN_TICKS)
					+ IntGenerator.generateRandomWithRange(0, Constants.DAY_IN_TICKS);
		}
	}

	// Getter/Setter-Methods
	
	@Override
	public GridPoint getGridpoint() {
		Grid<?> grid = (Grid<?>) ContextUtils.getContext(this).getProjection(Constants.GRID_NAME);
		return new GridPoint(grid.getLocation(this).getX(), grid.getLocation(this).getY());
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ProbedProperty(displayName = "Lagerzeit", usageName = "getStoreDuration", converter = "")
	public int getStoreDuration() {
		return storeDuration;
	}

	public void setStoreDuration(int storeDuration) {
		this.storeDuration = storeDuration;
	}

	@ProbedProperty(displayName = "Eingangszeitpunkt", usageName = "getEntryTime", converter = "")
	public long getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(int entryTime) {
		this.entryTime = entryTime;
	}

	@ProbedProperty(displayName = "Ausgangszeitpunkt", usageName = "getPickupTime", converter = "")
	public long getPickupTime() {
		return pickupTime;
	}

	public void setPickupTime(int pickupTime) {
		this.pickupTime = pickupTime;
	}
	
	public void setEntryTime(long entryTime) {
		this.entryTime = entryTime;
	}

	public void setPickupTime(long pickupTime) {
		this.pickupTime = pickupTime;
	}

	public boolean isSortingInProgress() {
		return sortingInProgress;
	}

	public void setSortingInProgress(boolean sortingInProgress) {
		this.sortingInProgress = sortingInProgress;
	}

	// Ende Getter/Setter-Methods
	
	public void step() {
		// 
		this.storeDuration = this.storeDuration - 1;
		Parameters params = RunEnvironment.getInstance().getParameters();
		Integer strategy = params.getInteger(Constants.STORAGE_STRATEGY);
		switch ( strategy ) {
			case 2:
			case 3:
				// Do not assign sorting jobs for strategy 2 and 3
				break;
			default:
				this.findRobot();
				break;
		}
	}

	public void findRobot() {
		// Checks if good is already being sorted.
		// 
		if ( this.sortingInProgress == true ) {
			return;
		}
		// Looking for a Robot that sorts the good.
		// Asking every day if we can find a free Robot.
		// If there is one it gets the job.
		if ( GoodStorageSystem.getInstance().getPickupQueue().contains(this)
				&& GoodStorageSystem.getInstance().getPickupQueue().peek() != this ) {
			Robot robot = AgentFinder.getClosestFreeRobot(this);
			// Check if a Robot was found, so its not null.
			if ( robot != null ) {
				GoodStorageSystem.getInstance().getPickupQueue().remove(this);
				robot.setRobotJobType(Robot.RobotJobType.SORT_JOB);
				robot.setTargetAgent(this);
				this.sortingInProgress = true;
			} else {
				ScheduleParameters sp = ScheduleParameters
						.createOneTime(RunEnvironment.getInstance().getCurrentSchedule().getTickCount() + 1);
				RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, "findRobot");
			}
		}
	}
	
	// toString method
	
	@ProbeID()
	public String toString() {
		return "Ware " + this.id;
	}

}
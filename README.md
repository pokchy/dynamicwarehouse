# This is the project by Salzstreuer group

## How to access this repository
* Make sure to have git installed

  https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

<br>

* Make sure to have Repast Symphony > 2.5.0 installed

  https://repast.github.io/download.html

<br>

* In you command line interface of choice

  $> cd \<directory where to save this project>

  $> git clone https://gitlab.com/pokchy/DynamicWarehouse.git

  $> cd \<path to DynamicWarehouse>

<br>

* Import repository into your eclipse workspace of choice